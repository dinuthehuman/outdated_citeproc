# citeproc-js-server-forked

A fork of Zoteros citeproc-js-server with the only difference of containing executable binaries created by [pkg](https://github.com/zeit/pkg) (and being a little outdated, probably).

This fork is inteded for easy use of citeproc-js-server in combination with our conversion software [Turandot](https://gitlab.com/solutionsbuero/turandot). Check the installation guide for more information.

For any detailed information on citeproc-js-server, please refer to the softwares [original repo on GitHub](https://github.com/zotero/citeproc-js-server).

## Used build commands

```bash
npm install .
pkg --target node8-linux-x64  --output ./linux_x64 .
pkg --target node8-win-x64 --output ./windows_x64.exe .
pkg --target node8-macos-x64 --output ./macos_x64 .
```

## Restrictions

- We currently build for Linux (tested on Ubuntu 18.04), Windows 10 and MacOS running on x64 CPUs, since Turandot is aimed at these architectures/OSes.
- The MacOS build is currently untested (for lack of a testing platform)
- The executables are **not** packed with all their assets and only run if they are left in the repos complete folder structure. Any help on that would be appreciated, though.

## Cloning

It is important to recursively download the submodules of this repository, so all the needed assets are included:

```bash
git clone --recurse-submodules https://gitlab.com/solutionsbuero/citeproc-js-server-forked.git
```
